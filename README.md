# F1 Wiki
A simple react (18.2.0) application using [tanstack query](https://github.com/TanStack/query) for state and caching.

API provided by the nice people @ http://ergast.com/mrd

## Why react query (tanstack query)
Statemangement
No fuss caching, that can be scaled

## Ramda
Because it gives me a functional approach to getting past javascript whoes and operators

## Getting started

Install required packages

`npm i` or `yarn`


Start the application

`npm run start` or `yarn start`
