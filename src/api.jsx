import { useQuery } from "@tanstack/react-query";
import fetch from "./fetch";
import fns from "./utils";

export const seasonsQuery = () => {
  return useQuery({
    queryKey: ["seasons"],
    queryFn: async () => {
      const resp = await fetch(
        "http://ergast.com/api/f1/driverStandings/1.json?offset=55"
      );
      return fns.standingsList(resp);
    },
  });
};

export const seasonQuery = (seasonId) => {
  return useQuery({
    queryKey: ["season", seasonId],
    queryFn: () => fetch(`http://ergast.com/api/f1/${seasonId}/results/1.json`),
  });
};


export const roundQuery = (seasonId, round) => {
  return useQuery({
    queryKey: ["season", seasonId, round],
    queryFn: () => fetch(`http://ergast.com/api/f1/${seasonId}/${round}/results/.json`),
  });
};
