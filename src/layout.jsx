import React from "react";
import { Switch, Route, NavLink, Redirect } from "react-router-dom";

// api calls
import { seasonsQuery } from "./api";

// Components
import Seasons from "./seasons";
import Season from "./season";
import Round from "./round";

export default function Layout() {
  const { status } = seasonsQuery();

  if (status === "loading") return <div className="w-1/6 m-auto">Loading...</div>;
  if (status === "error") return <div className="w-1/6 m-auto">Error :(</div>;

  return (
    <div className="w-full h-screen">
      <nav className="w-full flex flex-row p-2 bg-red-600">
        <NavLink to="/seasons" className="align-start mr-3">
          F1 Wiki
        </NavLink>
        <div className="flex justify-center w-auto">
          <NavLink
            to="/seasons"
            className={({ isActive }) => (isActive ? "text-gray-300" : "")}
          >
            <div className="text-white">Home</div>
          </NavLink>
        </div>
      </nav>
      <main className="w-full p-4">
        <Switch>
          <Route exact path="/seasons">
            <Seasons />
          </Route>
          <Route exact path="/season/:seasonId">
            <Season />
          </Route>
          <Route exact path="/season/:seasonId/:roundId">
            <Round />
          </Route>
          <Redirect to="/seasons" />
        </Switch>
      </main>
    </div>
  );
}
