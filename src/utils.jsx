import {
  isEmpty,
  isNil,
  pipe,
  head,
  path,
  pathOr,
  prop,
  equals,
  // length,
  not,
  split,
  props,
  // map,
  join,
  find,
  or,
  has,
} from "ramda";

// new alias: noLen + hasLen
// const isZeroLen = (subject) => equals(length(subject), 0);
// const notZeroLen = pipe(isZeroLen, not);
// deep path
const at = (pathAt, subject = {}) => path(split(".", pathAt), subject);
const atOr = (fallback, pathAt, subject = {}) =>
  pathOr(fallback, split(".", pathAt), subject);

// reduce multiple props value to string
const stringFromProps = (args = [], subject = {}) => {
  if (isEmpty(subject)) return "";
  return pipe(props(args), join(" "))(subject);
};

// helper fns to reduce path to props
const standingsList = (subject = {}) =>
  atOr([], "MRData.StandingsTable.StandingsLists", subject);

const raceList = (subject = {}) => atOr([], "MRData.RaceTable.Races", subject);

const champForSeason = (subject = {}, season) => {
  if (or(isNil(subject), isEmpty(subject))) return subject;

  const found = fns.find(
    (item) => fns.eq(fns.prop("season", item), season),
    subject
  );
  return has("DriverStandings", found)
    ? fns.head(fns.prop("DriverStandings", found))
    : {};
};

const fns = {
  isEmpty,
  isNil,
  notEmpty: pipe(isEmpty, not),
  notNil: pipe(isNil, not),
  head: head,
  prop,
  path: path,
  at: at,
  atOr: atOr,
  props,
  eq: equals,
  find,

  // 3rd Party
  stringFromProps,
  standingsList: standingsList,
  races: raceList,

  champ: champForSeason,
};

export default fns;
