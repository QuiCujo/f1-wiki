import React from "react";
import { useParams } from "react-router";
import { useQueryClient } from "@tanstack/react-query";
import { Link, useHistory } from "react-router-dom";

import fns from "./utils";

import { seasonQuery } from "./api";
export default function Season() {
  const { seasonId } = useParams();
  const queryClient = useQueryClient();
  const navigate = useHistory();
  // request for races in season
  let seasonsData = queryClient.getQueryData(["seasons"]);

  const { data, status } = seasonQuery(seasonId);

  if (status === "loading") return <div className="w-1/6 m-auto">Loading...</div>;
  if (status === "error") return <div className="w-1/6 m-auto">Error :(</div>;
  

  // filter for champ of current season
  const champ = fns.champ(seasonsData, seasonId); // filter for correct season

  return (
    <div className="w-5/6 m-auto">
      <div className="my-2">
        <div className="" role="button" tabIndex={0} onClick={() => navigate.goBack()} onKeyDown={() => navigate.goBack()}>
          &lt; Go back
        </div>
      </div>
      <div className="grid gap-2 grid-cols-2 lg:grid-cols-4 text-lg p-2 mb-2">
        <div>Race name</div>
        <div>Date</div>
        <div>Winner</div>
        <div>Constructor</div>
      </div>
      {
        // map over each race
        fns.races(data).map((race) => {
          const { Results, round, raceName, date } = race;
          const { Driver, Constructor } = fns.head(Results);
          const { driverId, givenName, familyName } = Driver;
          const { name } = Constructor;

          // highlight season champ
          return (
            <Link
              to={`/season/${seasonId}/${round}`}
              key={`season_${round}`}
              className={`grid gap-2 grid-cols-2 lg:grid-cols-4 text-lg p-2 mb-2 ${
                fns.eq(driverId, fns.at("Driver.driverId", champ))
                  ? "bg-red-100"
                  : ""
              }`}
            >
              <div>{raceName}</div>
              <div>{date}</div>
              <div>
                {givenName} {familyName}
              </div>
              <div>{name}</div>
            </Link>
          );
        })
      }
    </div>
  );
}
