import React from "react"
import { useParams } from "react-router"
import { useQueryClient } from "@tanstack/react-query"
import { useHistory } from "react-router-dom"
import { roundQuery } from "./api"
import fns from "./utils"
export default function Round() {
  // get our route params
  const { seasonId, roundId } = useParams()

  // used to get data aleady fetched + cached
  const queryClient = useQueryClient()

  // for navigating back
  const navigate = useHistory()

  // no need to refetch as its already cached
  let seasonsData = queryClient.getQueryData(["seasons"])

  // fetch round spesific data based on season and round
  const { data, status } = roundQuery(seasonId, roundId)

  if (status === "loading")
    return <div className="w-1/6 m-auto">Loading...</div>
  if (status === "error") return <div className="w-1/6 m-auto">Error :(</div>

  // filter for champ of current season
  const champ = fns.champ(seasonsData, seasonId)

  // round data
  const { Results } = fns.head(fns.races(data))

  const goBack =(event) => {
    event.preventDefault();
    navigate.goBack()
  }

  // make sure its not empty
  if (fns.isEmpty(Results))
    return <div>Oops seems like we&apos;ve gone off track.</div>

  return (
    <div className="w-5/6 m-auto">
      <div className="my-2">
        <div className="" role="button" tabIndex={0} key="round_goback" onClick={goBack} onKeyDown={goBack}>
          &lt; Go back
        </div>
      </div>
      <div className="grid gap-2 grid-cols-3 lg:grid-cols-6 text-lg p-2 mb-2">
        <div>Position</div>
        <div>No</div>
        <div>Driver</div>
        <div>Constructor</div>
        <div>Time</div>
        <div>Points</div>
      </div>

      {
        // map over each driver
        Results.map((race) => {
          const {
            Driver,
            Constructor,
            position,
            Time,
            status,
            number,
            points,
          } = race
          const { driverId, givenName, familyName } = Driver
          const { name } = Constructor

          // highlight season champ
          return (
            <div
              key={`season_${seasonId}_${driverId}`}
              className={`grid gap-2 grid-cols-3 lg:grid-cols-6 text-lg p-2 mb-2 ${
                fns.eq(driverId, fns.at("Driver.driverId", champ))
                  ? "bg-red-100"
                  : ""
              }`}
            >
              <div>{position}</div>
              <div>{number}</div>
              <div>
                {givenName} {familyName}
              </div>
              <div>{name}</div>
              <div>{fns.isNil(Time) ? status : fns.prop("time", Time)}</div>
              <div>{points}</div>
            </div>
          )
        })
      }
    </div>
  )
}
