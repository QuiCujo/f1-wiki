import React from "react";
import { useQueryClient } from "@tanstack/react-query";
import { Link } from "react-router-dom";
import fns from "./utils";

export default function Seasons() {
  const queryClient = useQueryClient();
  const seasonsData = queryClient.getQueryData(["seasons"]);

  return (
    <div className="w-5/6 m-auto">
      <div className="my-4 text-2xl">Welcome to the F1 Champions wiki.</div>
      <div className="w-full grid grid-cols-4 gap-2">
        {fns.notEmpty(seasonsData) ? (
          seasonsData.map((item) => {
            // deconstruct for use in template
            const { season, DriverStandings } = item;
            const { Driver, Constructors } = fns.head(DriverStandings);
            const { givenName, familyName } = Driver;
            const { name } = fns.head(Constructors);

            return (
              <Link to={`/season/${season}`} key={`season_${season}`}>
                <div className="flex flex-col p-2 m-1 bg-red-300 rounded">
                  <div className="flex justify-center text-lg">{season}</div>
                  <div className="flex justify-center text-xl">
                    {givenName} {familyName}
                  </div>
                  <div className="flex justify-center">{name}</div>
                  <div className="text-md text-center mt-2">
                    View more 
                  </div>
                </div>
              </Link>
            );
          })
        ) : (
          <div />
        )}
      </div>
    </div>
  );
}
